# Packaging of Cplex

Written the 28/04/2022 for cplex `22.1.0`, **instructions could be out of date** !

Cplex is incredibly annoying to install : you must download an opaque installer from IBM after creating a student/faculty account, and launch it which will litter the system with files.
This repository provides a way to create a *nice* package from an *already downloaded* installer.
I cannot include the installer in this repository as it would break the license agreement with IBM.

## Downloading the Cplex installer

1. Go to https://www.ibm.com/academic/topic/data-science
2. Create an IBM academic account (*IBM ID*) : you need to have a *student* or recognized *academic* email account.
   You must be logged in for the following steps.
   Tested institutions for permanent members :
    - Inria : does not work
    - INSA : works
3. At https://www.ibm.com/academic/topic/data-science ([screenshot](screenshot/ibm_page_data_science.png)), select the **software** tab and click on the `ILOG CPLEX Optimization Studio` entry, then on the `download` link.
4. On the [download page](screenshot/ibm_page_download.png) select the `linux x86_64` installer (if debian/ubuntu), then set `http` download, agree to the license, and finally download.

The installer can be installed as is in `/usr` (not recommended) or in a local home directory.
See below for creating a package with the installer files so that it can be easily updated / removed.

## Debian/ubuntu Packaging

The infrastructure in this repository can create a local debian package (`.deb`) that can be installed locally.
It should **never** be published anywhere as it would violate the IBM licensing agreement.
This has been tested on `Ubuntu 20.04 LTS`, and should work on any other debian-derived distribution.

```bash
apt install debhelper # debian packaging tools
apt install firejail # sandbox tool to prevent the installer from littering $HOME ; can be avoided by editing deb/debian/control and deb/Makefile
apt install <requested python versions> # python bindings are only packaged for the **installed** python versions, so install them beforehand

# Download the IBM cplex in the deb/ directory
mv path/to/cplex_installer deb/

# Build the debian package ; files will be generated at the git root.
cd deb/
debuild -uc -us

# Install package
dpkg --install ../cplex_22.10-1_amd64.deb

# To save disk space, cleanup can be done using :
dh clean # in deb/
git clean
```

## Update
- Readme dates and version
- version in deb/debian/changelog
- installer filename in deb/Makefile

## TODO
- user feedback
- python might break if multiple python versions are present, check
